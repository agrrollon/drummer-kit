//Detecting Button Press
let buttons = document.querySelectorAll(".drum").length;
for (let i = 0; i <= buttons; i++) {
    document.querySelectorAll(".drum")[i].addEventListener('click', function() {
        let buttonInnerHTML = this.innerHTML;
        
        makeSound(buttonInnerHTML);
        buttonAnimation(buttonInnerHTML);

    });

    //Detecting Keyboard Press

    document.addEventListener('keydown', function(e) {
    makeSound(e.key);
    buttonAnimation(e.key)

    document.addEventListener('keyup', function(e) {
        buttonUp(e.key);
    })
});

function makeSound(key) {
    switch (key) {
        case "w" :
            let crash = new Audio("assets/sounds/crash.mp3");
            crash.play();
        break;
        case "a" :
            let kick = new Audio("assets/sounds/kick-bass.mp3");
            kick.play();
        break;
        case "s" :
            let snare = new Audio("assets/sounds/snare.mp3");
            snare.play();
        break;
        case "d" :
            let tom1 = new Audio("assets/sounds/tom-1.mp3");
            tom1.play();
        break;
        case "j" :
            let tom2 = new Audio("assets/sounds/tom-2.mp3");
            tom2.play();
        break;
        case "k" :
            let tom3 = new Audio("assets/sounds/tom-3.mp3");
            tom3.play();
        break;
        case "l" :
            let tom4 = new Audio("assets/sounds/tom-4.mp3");
            tom4.play();
        break;
        default: 
            console.log(buttonInnerHTML)
    }
}

function buttonAnimation(currentKey) {
   let activeButton = document.querySelector("."+currentKey);
   activeButton.classList.add("pressed");

//    let inActiveButton = document.querySelector("."+currentKey);
//    inActiveButton.classList.remove("pressed");
}
function buttonUp(currentKey) {
    let inActiveButton = document.querySelector("."+currentKey);
    inActiveButton.classList.remove("pressed");
}

}

 

